FROM openjdk:8-jdk-alpine

RUN mkdir -p /opt/dynamic-etl-service

COPY target/dynamic-etl-service-1.0-SNAPSHOT.jar /opt/dynamic-etl-service/
WORKDIR /opt/dynamic-etl-service/

EXPOSE 8080

ENV JAVA_OPTS=""
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar dynamic-etl-service-1.0-SNAPSHOT.jar" ]