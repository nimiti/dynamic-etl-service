package com.yad2.etl.kafka.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.yad2.etl.model.GenericEntity;
import com.yad2.etl.model.GenericMsg;
import com.yad2.etl.model.responses.GenericResponse;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.kafka.sender.KafkaSender;
import reactor.kafka.sender.SenderOptions;
import reactor.kafka.sender.SenderRecord;
import reactor.kafka.sender.SenderResult;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by voxpopuli on 1/31/18.
 */
@Component
public class KafkaProducer {

    private static final Logger log = LoggerFactory.getLogger(KafkaProducer.class.getName());

    @Value("${kafka.input.topic}")
    private String inputTopic;

    @Value("${bootstrap.servers}")
    private String bootstrapServers;

    private KafkaSender<String, String> sender;

    @Autowired
    private ObjectMapper om;


    @PostConstruct
    public void init() {

        Map<String, Object> props = new HashMap<>();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        props.put(ProducerConfig.CLIENT_ID_CONFIG, "sample-producer");
        props.put(ProducerConfig.ACKS_CONFIG, "all");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        SenderOptions<String, String> senderOptions = SenderOptions.create(props);
        sender = KafkaSender.create(senderOptions);

    }


    public Mono<GenericResponse> sendFluxMessage(Mono<GenericMsg> msg) {
       Flux<GenericEntity> actionDTOFlux = msg
                .map(GenericMsg::generateUuids)
                .map(GenericMsg::getEntities)
                .flatMapMany(Flux::fromIterable);

        GenericResponse resDto = new GenericResponse();

       return sender.send(actionDTOFlux.flatMap(actionDTO -> {
            SenderRecord<String, String, Integer> record = SenderRecord.create(new ProducerRecord<>(
                            inputTopic,
                            actionDTO.getUuid(),
                            actionDTO.getJsonData(om)),
                    1);
            resDto.addEntity(actionDTO);
            return Flux.just(record)
                    .doOnError(e -> log.error("Send failed", e))
                    .onErrorResume(e -> Flux.empty());
        }))/*.doOnNext(System.out::print)*/.then(Mono.just(resDto));

    }

    @PreDestroy
    public void close() {
        sender.close();
    }

}
