package com.yad2.etl.configurations;

import com.yad2.etl.application.handlers.DataStreamHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RequestPredicates.POST;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration
public class RouterConfig {

    @Bean
    public RouterFunction<ServerResponse> monoRouterFunction(DataStreamHandler dataStreamHandler) {
        return route(POST("/pipeEvent"), dataStreamHandler::pipeEvent);
    }

}
