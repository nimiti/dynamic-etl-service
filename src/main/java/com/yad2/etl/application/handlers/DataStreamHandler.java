package com.yad2.etl.application.handlers;

import com.yad2.etl.kafka.services.KafkaProducer;
import com.yad2.etl.model.GenericEntity;
import com.yad2.etl.model.GenericMsg;
import com.yad2.etl.model.responses.GenericResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

@Component
public class DataStreamHandler {

    @Autowired
    private KafkaProducer kafkaProducer;

    public Mono<ServerResponse> pipeEvent(ServerRequest request) {
        Mono<GenericMsg> logDTOMono = request.bodyToMono(GenericMsg.class);
        Mono<GenericResponse> response = kafkaProducer.sendFluxMessage(logDTOMono);
        return ServerResponse.ok().body(response, GenericResponse.class);
    }

}
