package com.yad2.etl.model;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

public class GenericMsg implements Serializable {

    private String protocol;
    private List<GenericEntity> entities;

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public List<GenericEntity> getEntities() {
        return entities;
    }

    public void setEntities(List<GenericEntity> entities) {
        this.entities = entities;
    }

    public GenericMsg generateUuids() {
        getEntities().forEach(actionDTO -> {
            if (actionDTO.getUuid() == null && actionDTO.getAction().equals("new")){
                actionDTO.setUuid(UUID.randomUUID().toString());
            }
        });

        return this;
    }

    boolean needUuid(GenericEntity actionDTO) {
        return actionDTO.getUuid() == null && actionDTO.getAction().equals("new");
    }
}