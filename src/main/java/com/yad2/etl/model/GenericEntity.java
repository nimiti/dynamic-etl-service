package com.yad2.etl.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class GenericEntity implements Serializable {

    private String action;
    private Map<String, Object> data = new HashMap<>();
    private String uuid;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }

    public String getUuid() {
        return uuid;
    }

    public GenericEntity setUuid(String uuid) {
        this.uuid = uuid;
        return this;
    }


    public String getJsonData(ObjectMapper om) {
        String json = null;
        if (om != null) {
            try {
                json = om.writeValueAsString(data);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
        return json;
    }

    @Override
    public String toString() {
        return "GenericEntity{" +
                "action='" + action + '\'' +
                ", data='" + data + '\'' +
                ", uuid='" + uuid + '\'' +
                '}';
    }
}
