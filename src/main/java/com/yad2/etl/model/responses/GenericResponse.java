package com.yad2.etl.model.responses;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.yad2.etl.model.GenericEntity;

import java.util.ArrayList;
import java.util.List;

public class GenericResponse {

    private String resId;
    private List<GenericEntity> entities = new ArrayList<>();

    public String getResId() {
        return resId;
    }

    public void setResId(String resId) {
        this.resId = resId;
    }

    public List<GenericEntity> getEntities() {
        return entities;
    }

    public void setEntities(List<GenericEntity> entities) {
        this.entities = entities;
    }

    public GenericResponse addEntity(GenericEntity entity) {
        entities.add(entity);
        return this;
    }

}
